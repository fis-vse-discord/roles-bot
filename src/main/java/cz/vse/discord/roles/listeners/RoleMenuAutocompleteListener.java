package cz.vse.discord.roles.listeners;

import cz.vse.discord.roles.domain.MappedRoleMenu;
import cz.vse.discord.roles.repository.MappedRoleMenuRepository;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.events.interaction.command.CommandAutoCompleteInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.Command;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;

@Component
@AllArgsConstructor
public class RoleMenuAutocompleteListener extends ListenerAdapter {

    @NonNull
    private final MappedRoleMenuRepository repository;

    @Override
    public void onCommandAutoCompleteInteraction(@NonNull CommandAutoCompleteInteractionEvent event) {
        final var commands = Set.of(
                "bind-role",
                "unbind-role",
                "delete-role-menu",
                "create-subject",
                "create-announcement"
        );

        // TODO: Add caching to the repository to save resources
        if (commands.contains(event.getName()) && event.getFocusedOption().getName().equals("menu")) {
            final var guild = Objects.requireNonNull(event.getGuild()).getIdLong();
            final var names = repository.findAllByGuild(guild).stream()
                    .map(MappedRoleMenu::getName)
                    .map(name -> new Command.Choice(name, name))
                    .toList();

            event.replyChoices(names).queue();
        }
    }
}
