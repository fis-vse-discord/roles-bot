package cz.vse.discord.roles.listeners;

import cz.vse.discord.roles.discord.DiscordLogger;
import cz.vse.discord.roles.domain.Announcement;
import cz.vse.discord.roles.repository.AnnouncementRepository;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.UserSnowflake;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Objects;

@Component
@AllArgsConstructor
public class RoleButtonsListener extends ListenerAdapter {

    @NonNull
    private final AnnouncementRepository repository;

    @NonNull
    private final DiscordLogger logger;

    @Override
    public void onButtonInteraction(@NonNull ButtonInteractionEvent event) {
        // Only respond to the role:<id> button events
        if (!event.getComponentId().startsWith("role:")) {
            return;
        }

        final var id = event.getComponentId().replace("role:", "");

        final var guild = Objects.requireNonNull(event.getGuild());
        final var role = Objects.requireNonNull(guild.getRoleById(id));
        final var member = Objects.requireNonNull(event.getMember());

        if (member.getRoles().contains(role)) {
            guild.removeRoleFromMember(UserSnowflake.fromId(member.getId()), role).queue();
            event.replyEmbeds(new EmbedBuilder().setColor(0xED4245).setTitle("Role ti byla odebrána").build()).setEphemeral(true).queue();

            this.logger.log(guild, "The role " + role.getAsMention() + " was removed from " + member.getAsMention());

            return;
        }

        guild.addRoleToMember(UserSnowflake.fromId(member.getId()), role).queue();
        event.replyEmbeds(new EmbedBuilder().setColor(0x57F287).setTitle("Role ti byla přidělena").build()).setEphemeral(true).queue();

        this.logger.log(guild, "The role " + role.getAsMention() + " was added to " + member.getAsMention());

        // Send out all configured announcements
        this.repository.findAllByDiscordRole(guild.getIdLong(), role.getIdLong())
                .forEach(announcement -> this.processAnnouncement(announcement, guild, role, member));
    }

    private void processAnnouncement(@NonNull Announcement announcement, @NonNull Guild guild, @NonNull Role role, @NonNull Member member) {
        final var content = this.resolveTemplate(announcement.getTemplate(), role, member);

        final var channel = Objects.requireNonNull(guild.getTextChannelById(announcement.getChannel()));
        final var embed = new EmbedBuilder()
                .setDescription(content)
                .setImage(announcement.getImage())
                .setTimestamp(Instant.now())
                .build();

        channel.sendMessageEmbeds(embed).queue();
    }

    @NonNull
    private String resolveTemplate(@Nullable String template, @NonNull Role role, @NonNull Member member) {
        return Objects.requireNonNullElse(template, "")
                .replace("{member}", member.getEffectiveName())
                .replace("{mention}", member.getAsMention())
                .replace("{role}", role.getAsMention());
    }
}
