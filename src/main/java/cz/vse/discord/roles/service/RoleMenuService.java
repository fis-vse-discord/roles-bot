package cz.vse.discord.roles.service;

import cz.vse.discord.roles.domain.Announcement;
import cz.vse.discord.roles.domain.MappedRole;
import cz.vse.discord.roles.domain.MappedRoleMenu;
import cz.vse.discord.roles.repository.AnnouncementRepository;
import cz.vse.discord.roles.repository.MappedRoleMenuRepository;
import cz.vse.discord.roles.repository.MappedRoleRepository;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.buttons.Button;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class RoleMenuService {

    @NonNull
    private final MappedRoleMenuRepository menuRepository;

    @NonNull
    private final MappedRoleRepository roleRepository;

    @NonNull
    private final AnnouncementRepository announcementRepository;

    public void createRoleMenu(@NonNull String name, @NonNull TextChannel channel, @NonNull Message message) {
        final var guild = channel.getGuild();

        final var menu = new MappedRoleMenu(0, guild.getIdLong(), channel.getIdLong(), message.getIdLong(), name);
        final var saved = menuRepository.save(menu);

        this.updateRoleMenuEmbed(saved, message);
    }

    public void deleteRoleMenu(@NonNull Guild guild, @NonNull String name) {
        this.menuRepository.findByGuildAndName(guild.getIdLong(), name)
                .ifPresent(menu -> {
                    this.menuRepository.delete(menu);
                    this.roleRepository.deleteAllByMenu(menu.getId());

                    final var channel = Objects.requireNonNull(guild.getTextChannelById(menu.getChannel()));
                    final var message = channel.retrieveMessageById(menu.getMessage()).complete();

                    message.delete().queue();
                });
    }

    public void bindRoleToMenu(@NonNull Guild guild, @NonNull Role role, @NonNull String name) {
        this.menuRepository
                .findByGuildAndName(guild.getIdLong(), name)
                .ifPresent(menu -> {
                    this.roleRepository.save(new MappedRole(0, menu.getId(), role.getIdLong(), role.getName()));

                    final var channel = Objects.requireNonNull(guild.getTextChannelById(menu.getChannel()));
                    final var message = channel.retrieveMessageById(menu.getMessage()).complete();

                    this.updateRoleMenuEmbed(menu, message);
                });
    }

    public void unbindRoleFromMenu(@NonNull Guild guild, @NonNull Role role, @NonNull String name) {
        this.menuRepository
                .findByGuildAndName(guild.getIdLong(), name)
                .ifPresent(menu -> {
                    this.roleRepository.deleteAllByMenuAndRole(menu.getId(), role.getIdLong());

                    final var channel = Objects.requireNonNull(guild.getTextChannelById(menu.getChannel()));
                    final var message = channel.retrieveMessageById(menu.getMessage()).complete();

                    this.updateRoleMenuEmbed(menu, message);
                });
    }

    private void updateRoleMenuEmbed(@NonNull MappedRoleMenu menu, @NonNull Message message) {
        final var roles = roleRepository.findAllByMenu(menu.getId());
        final var buttons = createRoleButtons(roles);

        final var embed = new EmbedBuilder()
                .setTitle(menu.getName())
                .setDescription("Pro přidání/odebrání role klikni na tlačítka pod zprávou")
                .build();

        message.editMessageEmbeds(embed)
                .setContent("")
                .setComponents(buttons)
                .queue();
    }

    private List<ActionRow> createRoleButtons(@NonNull List<MappedRole> roles) {
        if (roles.isEmpty()) {
            final var placeholder = Button.secondary("~", "V tomto menu nejsou zatím propojeny žádné role").withDisabled(true);
            final var row = ActionRow.of(placeholder);

            return List.of(row);
        }

        final var buttons = roles.stream()
                .sorted(Comparator.comparing(MappedRole::getName))
                .map(role -> Button.secondary("role:" + role.getRole(), role.getName()))
                .toList();


        // Split all roles into groups of 5 buttons
        final var counter = new AtomicInteger();

        return buttons.stream()
                .collect(Collectors.groupingBy(x -> counter.getAndIncrement() / 5))
                .values()
                .stream()
                .map(ActionRow::of)
                .toList();
    }

    public void createAnnouncement(@NonNull String menu, long guild, long role, long channel, @NonNull String template, @Nullable String image) {
        this.roleRepository
                .findByDiscordAndMenu(guild, role, menu)
                .ifPresent(entity -> this.announcementRepository.save(
                        new Announcement(0, entity.getId(), channel, template, image)
                ));
    }
}
