package cz.vse.discord.roles;

import cz.vse.discord.roles.configuration.DiscordConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(DiscordConfiguration.class)
public class RolesBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(RolesBotApplication.class, args);
	}

}
