package cz.vse.discord.roles.commands;


import cz.vse.discord.roles.discord.DiscordCommandDispatcher;
import cz.vse.discord.roles.discord.DiscordSlashCommand;
import cz.vse.discord.roles.service.RoleMenuService;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.SlashCommandInteraction;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@AllArgsConstructor
public class CreateAnnouncementCommand implements DiscordSlashCommand {

    @NonNull
    private final RoleMenuService service;

    @NonNull
    @Override
    public String getName() {
        return "create-announcement";
    }

    @NonNull
    @Override
    public CommandData getDefinition() {
        return Commands.slash(this.getName(), "Create a new announcement binding for the selected role")
                .addOption(OptionType.STRING, "menu", "Menu to which the announcement should be bound to", true, true)
                .addOption(OptionType.ROLE, "role", "Role to which the announcement should be bound to", true)
                .addOptions(
                        new OptionData(OptionType.CHANNEL, "channel", "The channel to which the announcement should be posted")
                                .setRequired(true)
                                .setAutoComplete(false)
                                .setChannelTypes(ChannelType.TEXT)
                )
                .addOption(OptionType.STRING, "template", "Template for the embed content, supported placeholders are: {member}, {mention} and {role}", true)
                .addOption(OptionType.STRING, "image", "Image URL that will be included in the embed", false)
                .setDefaultPermissions(DiscordCommandDispatcher.getDefaultPermissions())
                .setGuildOnly(true);
    }

    @Override
    public void handle(@NonNull SlashCommandInteraction interaction) {
        final var reply = interaction.deferReply(true).complete();

        final var guild = Objects.requireNonNull(interaction.getGuild());

        final var menu = Objects.requireNonNull(interaction.getOption("menu")).getAsString();
        final var role = Objects.requireNonNull(interaction.getOption("role")).getAsRole();
        final var channel = Objects.requireNonNull(interaction.getOption("channel")).getAsChannel().asTextChannel();
        final var template = Objects.requireNonNull(interaction.getOption("template")).getAsString();
        final var image = interaction.getOption("image");

        this.service.createAnnouncement(
                menu,
                guild.getIdLong(),
                role.getIdLong(),
                channel.getIdLong(),
                template,
                image == null ? null : image.getAsString()
        );

        reply.editOriginalEmbeds(
            new EmbedBuilder()
                    .setTitle("Announcement created")
                    .addField("Role", role.getAsMention(), false)
                    .addField("Channel", channel.getAsMention(), false)
                    .addField("Template", "`" + template.replace("`", "") + "`", false)
                    .build()
        ).queue();
    }
}
