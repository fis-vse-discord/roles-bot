package cz.vse.discord.roles.commands.support;

import cz.vse.discord.roles.discord.DiscordLogger;
import cz.vse.discord.roles.discord.DiscordSlashCommand;
import cz.vse.discord.roles.service.RoleMenuService;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.SlashCommandInteraction;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
@AllArgsConstructor
public class CreateSubjectCommand implements DiscordSlashCommand {

    @NonNull
    private final RoleMenuService service;

    @NonNull
    private final DiscordLogger logger;

    @NonNull
    @Override
    public String getName() {
        return "create-subject";
    }

    @NonNull
    @Override
    public CommandData getDefinition() {
        return Commands.slash(this.getName(), "Create a new channel and role with appropriate permissions")
                .addOption(OptionType.STRING, "code", "Code of the subject", true)
                .addOption(OptionType.STRING, "name", "Name of the subject", true)
                .addOptions(
                        new OptionData(OptionType.CHANNEL, "category", "Channel category in which the subject channel should be created in")
                                .setRequired(true)
                                .setAutoComplete(false)
                                .setChannelTypes(ChannelType.CATEGORY)
                )
                .addOption(OptionType.STRING, "menu", "Role menu that the subject role should be bound to", false, true);
    }

    @Override
    public void handle(@NonNull SlashCommandInteraction interaction) {
        final var reply = interaction.deferReply(true).complete();

        final var guild = Objects.requireNonNull(interaction.getGuild());

        final var code = Objects.requireNonNull(interaction.getOption("code")).getAsString();
        final var name = Objects.requireNonNull(interaction.getOption("name")).getAsString();
        final var category = Objects.requireNonNull(interaction.getOption("category")).getAsChannel().asCategory();

        final var role = guild.createRole()
                .setName(code + " - " + name)
                .setMentionable(false)
                .setHoisted(false)
                .complete();

        final var channel = guild.createTextChannel(code + "-" + name, category)
                .addPermissionOverride(guild.getPublicRole(), Collections.emptyList(), List.of(Permission.VIEW_CHANNEL)) // public role = @everyone
                .addPermissionOverride(role,
                        List.of(
                                Permission.VIEW_CHANNEL,
                                Permission.MESSAGE_HISTORY,
                                Permission.MESSAGE_SEND,
                                Permission.MESSAGE_SEND_IN_THREADS
                        ),
                        Collections.emptyList()
                )
                .setTopic(name)
                .complete();

        // If a role menu was specified, bind the created role to it
        Optional.ofNullable(interaction.getOption("menu"))
                .map(OptionMapping::getAsString)
                .ifPresent(menu -> service.bindRoleToMenu(guild, role, menu));

        logger.log(guild, "✨ Subject created: " + code + " / " + name);

        reply.editOriginalEmbeds(
                new EmbedBuilder()
                        .setTitle("Subject role/channel created")
                        .addField("Role", role.getAsMention(), false)
                        .addField("Channel", channel.getAsMention(), false)
                        .build()
        ).queue();
    }
}
