package cz.vse.discord.roles.commands;

import cz.vse.discord.roles.discord.DiscordCommandDispatcher;
import cz.vse.discord.roles.discord.DiscordLogger;
import cz.vse.discord.roles.discord.DiscordSlashCommand;
import cz.vse.discord.roles.service.RoleMenuService;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.SlashCommandInteraction;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@AllArgsConstructor
public class UnbindRoleCommand implements DiscordSlashCommand {

    @NonNull
    private final RoleMenuService service;

    @NonNull
    private final DiscordLogger logger;

    @NonNull
    @Override
    public String getName() {
        return "unbind-role";
    }

    @NonNull
    @Override
    public CommandData getDefinition() {
        return Commands.slash(this.getName(), "Unbind a role from the selected role menu")
                .addOption(OptionType.STRING, "menu", "Name of the menu that should the role be unbound from", true, true)
                .addOption(OptionType.ROLE, "role", "Role that should be unbound from the selected role menu", true)
                .setDefaultPermissions(DiscordCommandDispatcher.getDefaultPermissions())
                .setGuildOnly(true);
    }

    @Override
    public void handle(@NonNull SlashCommandInteraction interaction) {
        final var reply = interaction.deferReply(true).complete();

        final var guild = Objects.requireNonNull(interaction.getGuild());
        final var menu = Objects.requireNonNull(interaction.getOption("menu")).getAsString();
        final var role = Objects.requireNonNull(interaction.getOption("role")).getAsRole();

        this.service.unbindRoleFromMenu(guild, role, menu);
        this.logger.log(guild, "\uD83D\uDD34 Role " + role.getAsMention() + " was unbound from the role menu `" + menu + "`");

        reply.editOriginalEmbeds(
                new EmbedBuilder()
                        .setTitle("Role menu updated")
                        .build()
        ).queue();
    }
}
