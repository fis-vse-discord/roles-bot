package cz.vse.discord.roles.commands;

import cz.vse.discord.roles.discord.DiscordCommandDispatcher;
import cz.vse.discord.roles.discord.DiscordLogger;
import cz.vse.discord.roles.discord.DiscordSlashCommand;
import cz.vse.discord.roles.service.RoleMenuService;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.SlashCommandInteraction;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@AllArgsConstructor
public class CreateRoleMenuCommand implements DiscordSlashCommand {

    @NonNull
    private final RoleMenuService service;

    @NonNull
    private final DiscordLogger logger;

    @NonNull
    @Override
    public String getName() {
        return "create-role-menu";
    }

    @NonNull
    @Override
    public CommandData getDefinition() {
        return Commands.slash(this.getName(), "Create a new role menu")
                .addOption(OptionType.STRING, "name", "Name of the role menu", true)
                .addOptions(
                        new OptionData(
                                OptionType.CHANNEL,
                                "channel",
                                "The channel in which the role menu should be created"
                        )
                        .setChannelTypes(ChannelType.TEXT)
                        .setAutoComplete(false)
                        .setRequired(true)
                )
                .setDefaultPermissions(DiscordCommandDispatcher.getDefaultPermissions())
                .setGuildOnly(true);
    }

    @Override
    public void handle(@NonNull SlashCommandInteraction interaction) {
        final var reply = interaction.deferReply(true).complete();

        final var name = Objects.requireNonNull(interaction.getOption("name")).getAsString();
        final var channel = Objects.requireNonNull(interaction.getOption("channel")).getAsChannel().asTextChannel();
        final var message = channel.sendMessage("...").complete();

        this.service.createRoleMenu(name, channel, message);
        this.logger.log(channel.getGuild(), "\uD83D\uDD2E Role menu created: " + name + " in " + channel.getAsMention());

        reply.editOriginalEmbeds(
                new EmbedBuilder()
                        .setTitle("Role menu created")
                        .setDescription(message.getJumpUrl())
                        .build()
        ).queue();
    }
}
