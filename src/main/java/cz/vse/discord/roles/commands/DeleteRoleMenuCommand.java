package cz.vse.discord.roles.commands;

import cz.vse.discord.roles.discord.DiscordCommandDispatcher;
import cz.vse.discord.roles.discord.DiscordLogger;
import cz.vse.discord.roles.discord.DiscordSlashCommand;
import cz.vse.discord.roles.service.RoleMenuService;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.SlashCommandInteraction;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@AllArgsConstructor
public class DeleteRoleMenuCommand implements DiscordSlashCommand {

    @NonNull
    private final RoleMenuService service;

    @NonNull
    private final DiscordLogger logger;

    @NonNull
    @Override
    public String getName() {
        return "delete-role-menu";
    }

    @NonNull
    @Override
    public CommandData getDefinition() {
        return Commands.slash(this.getName(), "Delete the selected role menu and all associated roles")
                .addOption(OptionType.STRING, "menu", "Name of the menu that should the role be unbound from", true, true)
                .setDefaultPermissions(DiscordCommandDispatcher.getDefaultPermissions())
                .setGuildOnly(true);
    }

    @Override
    public void handle(@NonNull SlashCommandInteraction interaction) {
        final var reply = interaction.deferReply(true).complete();

        final var guild = Objects.requireNonNull(interaction.getGuild());
        final var menu = Objects.requireNonNull(interaction.getOption("menu")).getAsString();

        this.service.deleteRoleMenu(guild, menu);
        this.logger.log(guild, "\uD83D\uDDD1️ Role menu deleted: " + menu);

        reply.editOriginalEmbeds(
                new EmbedBuilder()
                        .setTitle("Role menu deleted")
                        .build()
        ).queue();
    }
}
