package cz.vse.discord.roles.commands.support;

import cz.vse.discord.roles.discord.DiscordSlashCommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.entities.channel.concrete.Category;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.SlashCommandInteraction;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.requests.restaction.order.CategoryOrderAction;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Component
public class SortChannelsCommand implements DiscordSlashCommand {
    @NonNull
    @Override
    public String getName() {
        return "sort-channels";
    }

    @NonNull
    @Override
    public CommandData getDefinition() {
        return Commands.slash(this.getName(), "Sort channels by name in a given category")
                .addOptions(
                        new OptionData(OptionType.CHANNEL, "category", "Category in which the channels should be sorted")
                                .setRequired(true)
                                .setAutoComplete(false)
                                .setChannelTypes(ChannelType.CATEGORY)
                );
    }

    @Override
    public void handle(@NonNull SlashCommandInteraction interaction) {
        final var reply = interaction.deferReply().complete();
        final var category = Objects.requireNonNull(interaction.getOption("category")).getAsChannel().asCategory();

        final var channels = category.getTextChannels();
        final var sorted = channels.stream()
                .sorted(Comparator.comparing(TextChannel::getName))
                .toList();

        // Combine all sort operations into a single CategoryOrderAction
        final var combined = sorted.stream().collect(
                foldLeft(
                        category.modifyTextChannelPositions(),
                        (action, channel) -> (CategoryOrderAction) action.selectPosition(channel).moveTo(sorted.indexOf(channel))
                )
        );

        final var embed =
                new EmbedBuilder()
                        .setColor(0x57F287)
                        .setTitle("Channels sorted")
                        .build();

        combined.queue();
        reply.editOriginalEmbeds(embed).queue();
    }

    public static <A, B> Collector<A, ?, B> foldLeft(final B initial, final BiFunction<? super B, ? super A, ? extends B> function) {
        return Collectors.collectingAndThen(
                Collectors.reducing(Function.<B>identity(), a -> b -> function.apply(b, a), Function::andThen),
                mapper -> mapper.apply(initial)
        );
    }

}
