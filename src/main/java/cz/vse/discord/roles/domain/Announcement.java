package cz.vse.discord.roles.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Data
@Table("announcements")
@AllArgsConstructor
public class Announcement {

    @Id
    @Column("id")
    private int id;

    @Column("role_id")
    private int role;

    @Column("channel_id")
    private long channel;

    @NonNull
    @Column("template")
    private String template;

    @Nullable
    @Column("image_url")
    private String image;

}
