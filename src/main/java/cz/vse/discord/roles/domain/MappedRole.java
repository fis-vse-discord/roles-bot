package cz.vse.discord.roles.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;

@Data
@Table("roles")
@AllArgsConstructor
public class MappedRole {

    @Id
    @Column("id")
    private int id;

    @Column("menu_id")
    private int menu;

    @Column("role_id")
    private long role;

    @NonNull
    @Column("role_name")
    private String name;

}
