package cz.vse.discord.roles.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;

@Data
@Table("menus")
@AllArgsConstructor
public class MappedRoleMenu {

    @Id
    @Column("id")
    private int id;

    @Column("guild_id")
    private long guild;

    @Column("channel_id")
    private long channel;

    @Column("message_id")
    private long message;

    @NonNull
    @Column("menu_name")
    private String name;

}
