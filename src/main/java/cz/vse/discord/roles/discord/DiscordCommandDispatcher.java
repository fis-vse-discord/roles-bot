package cz.vse.discord.roles.discord;

import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.DefaultMemberPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class DiscordCommandDispatcher extends ListenerAdapter {

    @NonNull
    private final List<DiscordSlashCommand> commands;

    @NonNull
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @NonNull
    public static DefaultMemberPermissions getDefaultPermissions() {
        return DefaultMemberPermissions.enabledFor(Permission.MANAGE_SERVER);
    }

    public void registerSlashCommands(@NonNull JDA client) {
        // Register all slash commands from the application context
        client.updateCommands()
                .addCommands(commands.stream().map(DiscordSlashCommand::getDefinition).toList())
                .queue();

        client.addEventListener(this);
    }

    @Override
    public void onSlashCommandInteraction(@NonNull SlashCommandInteractionEvent event) {
        // Find the appropriate command handler and delegate the command execution to it
        commands.stream()
                .filter(command -> command.getDefinition().getName().equals(event.getName()))
                .findFirst()
                .ifPresent(command -> {
                    try {
                        command.handle(event);
                    } catch (Exception exception) {
                        logger.error("There was an error when handling a slash command", exception);

                        final var reply = event.isAcknowledged()
                                ? event.getHook()
                                : event.deferReply(true).complete();

                        reply.editOriginalEmbeds(
                                new EmbedBuilder()
                                        .setTitle("Sorry there was an error")
                                        .build()
                        ).queue();
                    }
                });
    }
}
