package cz.vse.discord.roles.discord;

import net.dv8tion.jda.api.interactions.commands.SlashCommandInteraction;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import org.springframework.lang.NonNull;

public interface DiscordSlashCommand {

    @NonNull
    String getName();

    @NonNull
    CommandData getDefinition();

    void handle(@NonNull SlashCommandInteraction interaction);

}
