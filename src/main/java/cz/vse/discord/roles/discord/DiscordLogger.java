package cz.vse.discord.roles.discord;

import cz.vse.discord.roles.configuration.DiscordConfiguration;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class DiscordLogger {

    @NonNull
    private final DiscordConfiguration configuration;

    public void log(@NonNull Guild guild, @NonNull String line) {
        final var channel = guild.getTextChannelById(configuration.getLogChannel());
        final var embed = new EmbedBuilder()
                .setTitle("Roles bot", "https://gitlab.com/fis-vse-discord/roles-bot")
                .setDescription(line)
                .build();

        if (channel != null) {
            channel.sendMessageEmbeds(embed).queue();
        }
    }
}
