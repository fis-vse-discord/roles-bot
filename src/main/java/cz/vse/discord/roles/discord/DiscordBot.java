package cz.vse.discord.roles.discord;

import cz.vse.discord.roles.configuration.DiscordConfiguration;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.hooks.EventListener;
import org.springframework.boot.CommandLineRunner;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DiscordBot implements CommandLineRunner {

    @NonNull
    private final DiscordConfiguration configuration;

    @NonNull
    private final DiscordCommandDispatcher dispatcher;

    @NonNull
    private final List<EventListener> listeners;

    @Override
    public void run(String... args) throws InterruptedException {
        final var client = JDABuilder.createDefault(configuration.getToken())
                .setActivity(Activity.playing("with roles"))
                .build()
                .awaitReady();


        // Register all slash commands and the commands dispatcher itself
        dispatcher.registerSlashCommands(client);

        // Register all event listeners from the application context
        listeners.stream()
                .filter(listener -> !(listener instanceof DiscordCommandDispatcher))
                .forEach(client::addEventListener);
    }
}
