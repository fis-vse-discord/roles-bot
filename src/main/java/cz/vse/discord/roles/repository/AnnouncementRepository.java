package cz.vse.discord.roles.repository;

import cz.vse.discord.roles.domain.Announcement;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnnouncementRepository extends CrudRepository<Announcement, Integer> {

    @NonNull
    @Query("""
                select announcements.* from announcements
                    left join roles on announcements.role_id = roles.id
                    left join menus on roles.menu_id = menus.id
                    where menus.guild_id = :discord_guild and
                          roles.role_id = :discord_role
            """)
    List<Announcement> findAllByDiscordRole(
            @Param("discord_guild") long guild,
            @Param("discord_role") long role
    );

}
