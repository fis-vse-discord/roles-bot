package cz.vse.discord.roles.repository;

import cz.vse.discord.roles.domain.MappedRole;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MappedRoleRepository extends CrudRepository<MappedRole, Integer> {

    @NonNull
    List<MappedRole> findAllByMenu(int menu);

    @Modifying
    @Query("delete from roles where menu_id = :menu")
    void deleteAllByMenu(@Param("menu") int menu);

    @Modifying
    @Query("delete from roles where menu_id = :menu and role_id = :role")
    void deleteAllByMenuAndRole(@Param("menu") int menu, @Param("role") long role);

    @Query("""
    select roles.* from roles
        left join menus on roles.menu_id = menus.id
        where
            roles.role_id = :discord_role and
            menus.guild_id = :discord_guild and
            menus.menu_name = :menu_name
    """)
    Optional<MappedRole> findByDiscordAndMenu(
            @Param("discord_guild") long guild,
            @Param("discord_role") long role,
            @Param("menu_name") @NonNull String menu
    );
}
