package cz.vse.discord.roles.repository;

import cz.vse.discord.roles.domain.MappedRoleMenu;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MappedRoleMenuRepository extends CrudRepository<MappedRoleMenu, Integer> {

    @NonNull
    List<MappedRoleMenu> findAllByGuild(long idLong);


    @NonNull
    Optional<MappedRoleMenu> findByGuildAndName(long guild, @NonNull String name);

}
